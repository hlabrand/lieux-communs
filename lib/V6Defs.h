!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! V6defs.h
!! Include this file BEFORE the standard Inform Library
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! V6Lib
!! Copyright 1997-2000 Jason C. Penney (jpenney@chelmsford.com)
!! May be freely redistributed, as long as it is unchanged
!! May be used in any non-commercial game as long as V6Lib version
!!  info is displayed in the game.  By default the library does this
!!  itself via the Version function included in V6.h.
!! May be used in any commercial game with my written permission.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Message "[Including <V6Defs>]";

#Ifndef V6Defs_h;
Constant V6Defs_h;

System_file;

Constant V6LibVersion "0.826 (public beta)";

!Fonts
Constant FN_PREV    0;
Constant FN_NORMAL  1;
Constant FN_PICTURE 2;
Constant FN_GRAPHIC 3;
Constant FN_COURIER 4;

!Styles
Constant ST_ROMAN   0;
Constant ST_REVERSE 1;
Constant ST_BOLD    2;
Constant ST_ITALIC  4;
Constant ST_FIXED   8;

!colours
Constant C_BLACK   2;
Constant C_RED     3;
Constant C_GREEN   4;
Constant C_YELLOW  5;
Constant C_BLUE    6;
Constant C_MAGENTA 7;
Constant C_CYAN    8;
Constant C_WHITE   9;
Global   C_GREY =  10;
!Global   C_LGREY = 10;
!Global   C_GREY  = 11;
!Global   C_DGREY = 12;
Constant C_UNDERCUR -1;

!machine Types
Constant MT_DECSystem20_1 1;
Constant MT_Apple_IIe     2;
Constant MT_Macintosh     3;
Constant MT_Amiga         4;
Constant MT_Atari_ST      5;
Constant MT_IBM_PC        6;
Constant MT_Commodore_128 7;
Constant MT_Commodore_64  8;
Constant MT_Apple_IIc     9;
Constant MT_Apple_IIgs   10;
Constant MT_Tandy_Color  11;
!! This was suggested, it may never be implemented, or it may not be
!! the correct number... but it's here for now.
Constant MT_Portable    255;

!globals
Global MachineType = 0;
Array baddr_tmp --> 0 0 0 0;


!replace directives
#ifdef V6SCOTT;
Constant NODOMENU;
#ifnot;
replace DrawStatusLine;
replace Banner;
replace VersionSub;
replace SaveSub;
replace DoMenu;
#endif;

!other includes

#ifndef NOZPIC;
include "ZPicDefs";
#endif;

include "ZWinDefs";

#ifndef NOZSND;
include "ZSndDefs";
#endif;



[ Box__Routine maxw table n w w2 line lc t jcp oldzw;
    oldzw = ActiveZWindow;

    give ActiveZWinStyle ~general;
    n = table --> 0;
    @add n 6 -> jcp;
    
    jcp = jcp * 0->$26;
    
    @split_window jcp;
    !@set_window 1;
    StatusWin.Activate();
    w = 0 -> 33;
    if (w == 0) w=80;
    w2 = (w - maxw)/2;
    !style reverse;
    ActiveZWindow.SetFontStyle(ST_FIXED);
    @sub w2 2 -> w;
    line = 5;
    lc = 1;
    !@set_cursor 4 w;
    ActiveZWindow.SetCursorByChar(4,w);
    spaces maxw + 4;
    do
    {
	!@set_cursor line w;
	ActiveZWindow.SetCursorByChar(line,w);
	spaces maxw + 4;
	!@set_cursor line w2;
	ActiveZWindow.SetCursorByChar(line,w2);
	t = table --> lc;
	if (t~=0) print (string) t;
	line++; lc++;
    } until (lc > n);
    !@set_cursor line w;
    ActiveZWindow.SetCursorByChar(line,w);
    spaces maxw + 4;
    @buffer_mode 1;
    style roman;
    @set_window 0;
    @split_window 1;
    @output_stream $ffff;
    print "[ ";
    lc = 1;
    do
    {
	w = table --> lc;
	if (w ~= 0) print (string) w;
	lc++;
	if (lc > n)
	{
	    print "]^^";
	    break;
	}
	print "^  ";
    } until (false);
    @output_stream 1;
    oldzw.Activate();
    
];

#Endif;
