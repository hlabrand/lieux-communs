!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! V6.h
!!
!! Main header for my V6 library.
!! Should be included after the standard inform library.
!! Assumes that V6Defs.h was included BEFORE the standard inform
!!   library.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! V6Lib
!! Copyright 1997-2000 Jason C. Penney (jpenney@chelmsford.com)
!! May be freely redistributed, as long as it is unchanged
!! May be used in any non-commercial game as long as V6Lib version
!!  info is displayed in the game.  By default the library does this
!!  itself via the Version function included in V6.h.
!! May be used in any commercial game with my written permission.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Message "[Including <V6>]";
#Ifndef V6_h;
Constant V6_h;

#Ifndef V6Defs_h;
Message error "V6Defs needs to be included before V6";
#Endif;

#ifndef NOZPIC;
!!!!include "ZPic";
[ ZPicInit ;
    @picture_data 0 baddr_tmp ?ZPicInitContinue;
    .ZPicInitContinue;
    ZPicMax = baddr_tmp-->0;
    ZPicVersion = baddr_tmp-->1;
];
#Endif;	

!!!!include "ZWindow";
ZWindow StatusWin "StatusWin"
    private winnum 1;

ZWindow MainWin "MainWin"
    private winnum 0;

ZWindow ZWin2 "ZWin2"
    private winnum 2;

ZWindow ZWin3 "ZWin3"
    private winnum 3;

ZWindow ZWin4 "ZWin4"
    private winnum 4;

ZWindow ZWin5 "ZWin5"
    private winnum 5;

ZWindow ZWin6 "ZWin6"
    private winnum 6;

ZWindow ZWin7 "ZWin7"
    private winnum 7;


#ifndef V6SCOTT;
Object 	DefaultZWinStyle
 class 	ZWinStyle,
 with	Init [ tmp;
	    tmp = StatusWin.GetCharHeight();
	    tmp = tmp + (tmp/4);
	    @split_window tmp;
	    StatusWin.SetColours(MainWin.GetBGColour(),
				MainWin.GetFGColour());
	    StatusWin.SetFontStyle(ST_ROMAN);
	    MainWin.SetFontStyle(ST_ROMAN);
	],	
    	Update [ charh charw width posa posb oldwin;
	    !!   Default statusline routine
	    !!   based on inform library DrawStatusLine
	    
	    oldwin = ActiveZWindow;    
	    StatusWin.Activate();
	    StatusWin.Erase();
	    
	    charh = StatusWin.GetCharHeight();
	    charw = StatusWin.GetCharWidth();
	    width = (StatusWin.GetXSize() / charw);
	    posa = (width - 26) * charw;
	    posb = (width - 13) * charw;
	    
	    StatusWin.SetCursor((1+(charh/8)),charw);
	    print (name) location;
	    if ((01->1)&2 == 0)
	    {
		if (width > 76)
		{
		    StatusWin.SetCursor((1+(charh/8)),posa);    
		    print (string) SCORE__TX, sline1;
		    StatusWin.SetCursor((1+(charh/8)),posb);
		    print (string) MOVES__TX, sline2;
		}
		if (width > 63 && width <= 76)
		{
		    StatusWin.SetCursor((1+(charh/8)),posb);
		    print sline1, "/", sline2;
		}
	    }
	    else
	    {
		StatusWin.SetCursor((1+(charh/8)),posa);
		print (string) TIME__TX;
		LanguageTimeOfDay(sline1, sline2);
	    }
	    oldwin.Activate();
	];
#endif;

#ifndef NOZSND;
!!!!!include "ZSnd";
ZSnd HighBeep
 private sndnum 1;

ZSnd LowBeep
 private sndnum 2;


!Fade Stuff

Zsnd  	Fade_tmp
 private fd_sv 0,
    	fd_fv 0,
    	fd_steps 0,
    	fd_vdiff 0,
    	fd_step 0,
 with	Setup[ sv fv steps num;
	    !ZSnd.copy(self,snd); !Boy did that not work as expected!
	    self.sndnum = num;
	    if (sv > 8)
		sv = 8;
	    self.fd_sv = sv;
	    if (fv > 8)
		fv = 8;
	    self.fd_fv = fv;
	    self.fd_step = 0;
	    self.fd_steps = steps;
	],
	Fader [ steper;
	    if (self.fd_step < self.fd_steps)
	    {
		steper = (self.fd_steps - 1) / (self.fd_fv - self.fd_sv);
	    	if (steper)
	    	{
		    self.SetVolume(self.fd_sv + (self.fd_step/steper));
		}
		else
		{
		    self.SetVolume(self.fd_sv +
				   (self.fd_step *
				    ((self.fd_fv - self.fd_sv) /
				     (self.fd_steps - 1))));
		}
		self.fd_step++;
		self.Play(0,DoFade);
	    }
	];
	

[ DoFade ;
    Fade_tmp.Fader();
];

#Endif;

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! V6Init()
!!   basic game settings get initialized here
!!   Should be called first in Initilize Routine
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

[ V6Init style x;
!    pretty_flag = 0;
    MainWin.Activate();
    #ifndef NOZPIC;
    ZPicInit();
    #endif;
    MachineInit();
    ! needed to prevent calling of (-1).Finish()
    ActiveZWinStyle=DefaultZWinStyle;
    if (style == 0)
    	style = DefaultZWinStyle;
    style.Activate();
    ActiveZWinStyle.Init();
    ! @erase_window -1;
    for (x=0:x<8:x++)
    {
	@erase_window x;
    }
    give ActiveZWinStyle ~general;    
];


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! MachineInit()
!!   sets the MachineType global
!!   sets correct color values for Amiga
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
[ MachineInit ;
    MachineType = $1e->0;
    if (MachineType == MT_Amiga)
    {
	!C_LGREY = 10;
	C_GREY = 11;
	!C_DGREY = 12;
    }
];

#ifndef V6SCOTT;
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! replacement for DrawStatusLine
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
[ DrawStatusLine ;
    
    if ((0->$10) & $$00000010)
    {
	print "^^!NOTE!  bit set!!^^^";
	(0->$10) = ((0->$10) & $$11111101);
	give ActiveZWinStyle ~general;
    }
    
    if (ActiveZWinStyle hasnt general)
    {
	ActiveZWinStyle.Init();
	give ActiveZWinStyle general;
    }
    ActiveZWinStyle.Update();
];


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Banner and VersionSub replacements
!! based on Inform Library Version 6/10
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
[ Banner i;
    if (Story ~= 0)
    {
	#IFV5; style bold; #ENDIF;
   	print (string) Story;
	#IFV5; style roman; #ENDIF;
    }
    if (Headline ~= 0)
       	print (string) Headline;
    print "Release ", (0-->1) & $03ff;
    #ifndef NOZPIC;
    print " / Pix ", ZPicVersion;
    #endif;
    print " / Serial number ";
    for (i=18:i<24:i++) print (char) 0->i;
    print " / Inform v"; inversion;
    print " Library ", (string) LibRelease;
#ifdef STRICT_MODE;
   print "S";
#endif;
#ifdef INFIX;
   print "X";
#ifnot;
#ifdef DEBUG;
    print " D";
#endif;
    new_line;
];

[ VersionSub;
  Banner();
  if (standard_interpreter > 0)
      print "Standard interpreter ",
          standard_interpreter/256, ".", standard_interpreter%256,
          " (", 0->$1e, (char) 0->$1f, ") / ";
  else print "Interpreter ", 0->$1e, " Version ", (char) 0->$1f, " / ";
  print "Library serial number ", (string) LibSerial, "^";
#IFDEF LanguageVersion;
  print (string) LanguageVersion, "^";
#ENDIF;
  print "V6Lib Version ", (string) V6LibVersion, "^";
];

!!!!!!!!!!!!!!!!!!!!!!!!
!SaveSub
!
!!!!!!!!!!!!!!!!!!!!!!!!
[ SaveSub;
    give ActiveZWinStyle ~general;
    save Smaybe;
    return L__M(##Save,1);
    .SMaybe; L__M(##Save,2);
];


!!!!!!!!!!!!!!
!New DoMenu()
! Based on Inform Library
!!!!!!!!!!!!!!

#IfNDef NODOMENU;
!!!!!!!!!!!!!!!!!!
! allowing for an updated version of of L. Ross Raszewski's DOMENU
! replacement to work.  If there ever is an V6Lib compatible version.
! It would have to be included before V6.h
!!!!!!!!!!!!!!!!!!
[ DoMenu menu_choices EntryR ChoiceR
    lines main_title main_wid cl i j oldcl pkey;
    
    if (pretty_flag==0)
	return LowKey_Menu(menu_choices,EntryR,ChoiceR);
    give ActiveZWinStyle ~general;
    menu_nesting++;
    menu_item=0;
    lines=indirect(EntryR);
    main_title=item_name;
    main_wid=item_width;
    cl=7;
    StatusWin.HideCursor();
    .ReDisplay;
    oldcl=0;
    @erase_window $ffff;
    i=lines+7;
    StatusWin.SetFontStyle(ST_ROMAN);
    StatusWin.SetFontStyle(ST_FIXED);
    i = StatusWin.GetCharHeight() * i;
    @split_window i;

    !i = 0->33;
    i = 0->$21;
    if (i==0)
	i=80;

    !@set_window 1;
    !@set_cursor 1 1;
    !style reverse;
    !StatusWin.SetFontStyle(ST_REVERSE);
    StatusWin.Activate();
    StatusWin.SetCursorByChar(1,1);
    StatusWin.SetColours(MainWin.GetFGColour(),
			 MainWin.GetBGColour());
    StatusWin.Erase();


    StatusWin.SetFontStyle(ST_FIXED|ST_REVERSE);
    i = StatusWin.GetXSize()/StatusWin.GetCharWidth();
    spaces(i);
    j=i/2-main_wid;
    !@set_cursor 1 j;
    StatusWin.SetCursorByChar(1,j);
    print (string) main_title;
    !@set_cursor 2 1;
    StatusWin.SetCursorByChar(2,1);
    spaces(i);
    !@set_cursor 2 2;
    StatusWin.SetCursorByChar(2,2);
    print (string) NKEY__TX;
    j=i-12;
    !@set_cursor 2 j;
    StatusWin.SetCursorByChar(2,j);
    print (string) PKEY__TX;
    !@set_cursor 3 1;
    StatusWin.SetCursorByChar(3,1);
    spaces(i);
    !@set_cursor 3 2;
    StatusWin.SetCursorByChar(3,2);
    print (string) RKEY__TX;
    j=i-17;
    !@set_cursor 3 j;
    StatusWin.SetCursorByChar(3,j);
    if (menu_nesting==1)
	print (string) QKEY1__TX;
    else
	print (string) QKEY2__TX;
    !style roman;
    !@set_cursor 5 2;
    !StatusWin.SetCursorByChar(5,2);
    !font off;
    StatusWin.SetFontStyle(ST_ROMAN);
    StatusWin.SetFontStyle(ST_FIXED);
    StatusWin.SetCursorByChar(5,2);
    
    if (menu_choices ofclass String)
	print (string) menu_choices;
    else
	menu_choices.call();



    for (::)
    {
	if (cl ~= oldcl)
    	{
	    if (oldcl>0)
	    {
		!@set_cursor oldcl 4;
		StatusWin.SetCursorByChar(oldcl, 4);
		print " ";
	    }
	    !@set_cursor cl 4;
	    StatusWin.SetCursorByChar(cl, 4);
	    print ">";
    	}
	oldcl=cl;
	@read_char 1 -> pkey;
	if (pkey==NKEY1__KY or NKEY2__KY or 130)
	{
	    cl++;
 	    if (cl==7+lines)
		cl=7;
	    continue;
	}
	if (pkey==PKEY1__KY or PKEY2__KY or 129)
	{
	    cl--;
	    if (cl==6)
		cl=6+lines;
	    continue;
	}
	if (pkey==QKEY1__KY or QKEY2__KY or 27 or 131)
	    break;
	if (pkey==10 or 13 or 132)
	{
	    !@set_window 0;
	    MainWin.Activate();
	    !font on;
	    new_line;
	    new_line;
	    new_line;
    	    
	    menu_item=cl-6;
	    EntryR.call();
	    @erase_window $ffff;
	    !@split_window 1;
	    i = StatusWin.GetCharHeight();
	    i = i + (i/4);
	    @split_window i;
	    !i = 0->33;
	    i = StatusWin.GetXSize() / StatusWin.GetCharWidth();
	    if (i==0)
	    {
		i=80;
	    }
	    !@set_window 1;
	    !@set_cursor 1 1;
	    StatusWin.Activate();
	    StatusWin.SetCursor(1,1);
	    !style reverse;
	    !spaces(i);
	    StatusWin.SetColours(StatusWin.GetBGColour(),
				 StatusWin.GetFGColour());
	    StatusWin.Erase();
	    j=i/2-item_width;
	    !@set_cursor 1 j;
	    StatusWin.SetCursor((1+(StatusWin.GetCharHeight()/8)),
				(j * StatusWin.GetCharWidth()));
	    print (string) item_name;
	    !style roman;
	    !@set_window 0;
	    MainWin.Activate();
	    new_line;
    	    
	    i = ChoiceR.call();
	    if (i==2)
		jump ReDisplay;
	    if (i==3)
		break;
    	    
	    L__M(##Miscellany, 53);
	    @read_char 1 -> pkey;
	    jump ReDisplay;
	}
    }

    menu_nesting--;
    if (menu_nesting>0)
	rfalse;
    !font on;
    !@set_cursor 1 1;
    @erase_window $ffff;
    !@set_window 0;
    MainWin.Activate();
    !MainWin.Erase();
    ActiveZWinStyle.Activate();    
    new_line;
    new_line;
    new_line;
    if (deadflag==0)
	<<Look>>;
];  
#Endif;
!Skipitem
!!End DoMenu

#endif;!V6SCOTT

#Endif;!file ender

!get rid of needless compiler warnings
#Ifndef ZWinDefs_h;
#Endif;
#Ifndef ZPicDefs_h;
#Endif;
#Ifndef ZSndDefs_h;
#Endif;
#Ifndef RP_FOREVER;
#Endif;
#Ifndef V6_h;
#Endif;




