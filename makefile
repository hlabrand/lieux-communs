# Makefile pour centraliser les commandes sous Linux
#
# http://informfr.tuxfamily.org/
# http://ifiction.free.fr/
#

info:
	# make: + distrib / solution / zmachine / zdebug / glulx / blb / glulxmini clean 
	
all:    zmachine glulx blb 

distrib: solution

	mkdir -p lieuxcommuns
	mkdir -p lieuxcommuns/solutions
	mkdir -p lieuxcommuns/src
	cp -fr outils_win/gargoyle_inform lieuxcommuns
	rm -fr lieuxcommuns/gargoyle_inform/.svn/
	cp lieuxcommuns.z8 lieuxcommuns
	cp media/lieuxcommuns.blb lieuxcommuns
	cp garglk.ini lieuxcommuns
	cp README_Lisezmoi.txt lieuxcommuns
	cp README_Lisezmoi.html lieuxcommuns
	cp media/presentation_lieux_communs.pdf lieuxcommuns
	cp solution.txt lieuxcommuns/solutions
	# cp media/transcript.txt lieuxcommuns/solutions
	cp media/transcript.html lieuxcommuns/solutions
	cp *.rec lieuxcommuns/solutions
	cp *.inf  lieuxcommuns/src
	cp makefile  lieuxcommuns/src
	cp -fr lib     lieuxcommuns/src
	rm -fr lieuxcommuns/src/lib/.svn

	echo "start gargoyle_inform\glulxe.exe lieuxcommuns.blb" > lieuxcommuns/_jouer_LIEUX_COMMUNS_multimedia.bat
	echo "start gargoyle_inform\gargoyle.exe lieuxcommuns.z8" > lieuxcommuns/_jouer_LIEUX_COMMUNS_texte.bat

	cp outils_win/Lieux\ Communs.exe lieuxcommuns/

	rm -fr lieuxcommuns.zip
	zip lieuxcommuns.zip  -r lieuxcommuns 


solution:

	echo -e "* Ce fichier contient la solution g�n�rale du jeu. \n\
	* LIEUX COMMUNS  (http://ifiction.free.fr) \n\
	* Pour les sc�nes individuelles, veuillez vous r�f�rer \n\
	* aux fichiers de type scene##.rec \n\
	* (La commande 'replaymode' est uniquement utile \n\
	* lors de l'initialisation des sc�nes individuelles)" > solution.txt
	echo -e "\nreplaymode" >> solution.txt
	echo -e "\nnord \nentrer" >> solution.txt
	echo -e "\nfouiller ouvrages\nlire le livre\nregarder sous l'armoire\nfouiller le tiroir\nregarder" >> solution.txt
	echo -e "\ntoucher le galet" >> solution.txt
	cat scene39.rec >> solution.txt
	echo -e "\nsecouer le globe de neige" >> solution.txt
	cat scene31.rec >> solution.txt
	echo -e "\nprendre la carte postale"  >> solution.txt
	cat scene190.rec >> solution.txt
	echo -e "\nprendre le pendentif de corail"  >> solution.txt
	cat scene125.rec >> solution.txt
	echo -e "\nprendre l'ourson en peluche"  >> solution.txt
	cat scene86.rec >> solution.txt
	echo -e "\nsentir le vieux livre"  >> solution.txt
	cat scene37.rec >> solution.txt
	echo -e "\ntourner le sablier"  >> solution.txt
	cat scene172.rec >> solution.txt
	echo -e "\nfouiller debris" >> solution.txt
	echo -e "\nmettre ocarina sur autel \nmettre statuette sur autel \nmettre bandelette sur autel \nmettre pendentif de corail sur autel \nmettre couteau sur autel \nmettre chaine sur autel" >> solution.txt
	cat solution.txt | tr -d '\r' > solution_unix.txt
	unix2dos solution_unix.txt solution.txt
	#nawk 'sub("$", "\r")' solution_unix.txt > solution.txt
	#cp solution.txt solution_unix.txt
	#dos2unix -d solution_unix.txt


zmachine:

	chmod u+x outils_linux/inform-6.31-biplatform && \
	outils_linux/inform-6.31-biplatform  -v8 \
	+language_name=French1PSP \
	+include_path=./,./lib cpb.inf lieuxcommuns.z8 -x '$$MAX_DICT_ENTRIES=5000'

zdebug:

	chmod u+x outils_linux/inform-6.31-biplatform && \
	outils_linux/inform-6.31-biplatform  -v8 -D \
	+language_name=French1PSP \
	+include_path=./,./lib cpb.inf lieuxcommuns.z8 -x '$$MAX_DICT_ENTRIES=5000'
	
	
z8 : zmachine

zcode : zmachine

z5:
	# "pour tester si cela passe toujours en z5"
	chmod u+x outils_linux/inform-6.31-biplatform && \
	outils_linux/inform-6.31-biplatform  -v5 \
	+language_name=French1PSP \
	+include_path=./,./lib cpb.inf lieuxcommuns.z5 -x '$$MAX_DICT_ENTRIES=5000'

glulx:

	chmod u+x outils_linux/inform-6.31-biplatform && \
	outils_linux/inform-6.31-biplatform  -G  \
	+language_name=French1PSP \
	+include_path=./,./lib cpb.inf  -x '$$MAX_DICT_ENTRIES=5000' media/cpb.ulx

glulxmini:
# version pour pocket pc ou petit �cran. Bien g�n�rer le fichier avec make glulx auparavant
	echo "maintenant tapez : cd media puis ./make_glulxmini.sh"
#	for A in *.jpg ; do convert $A -resample 40%  /tmp/lieuxcommuns/media/$A ; done
#	for B in *.png ; do convert $B -resample 40%  /tmp/lieuxcommuns/media/$B ; done
#	cp *.ogg /tmp/lieuxcommuns/media/
#	cp cpb.res /tmp/lieuxcommuns/media/
#	cp cpb.ulx /tmp/lieuxcommuns/media/
#	cp gblorb.ulx /tmp/lieuxcommuns/media/
#	echo "cd /tmp/lieuxcommuns/media/ ; gargoyle /tmp/lieuxcommuns/media/gblorb.ulx" 

	
blb:
	#sh genere_blb.sh 
	make bresc
	


bresc:
	bresc media/cpb.res 
	mv media/cpb.blb media/lieuxcommuns.blb

clean:
	-rm -fr *~ 
	-rm -fr .*~ 
	-rm -fr lib/*~
	-rm -fr lib/.*~  

	
	
