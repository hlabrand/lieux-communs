Lieux communs
une fiction interactive
http://informfr.tuxfamily.org/


%!target: xhtml
%!options: --toc --outfile README_Lisezmoi.html
%!encoding: iso-8859-1
%!style: style.css
%!postproc: '<head>'	'<head><style type="text/css"> body { background-color: #FBF6EE ; color: black ; font-family: verdana, Arial; font-size: 12px;}  a:link,a:active,a:visited {color: #331111; text-decoration: none;border-bottom: thin dotted #99aaaa ;}  a:hover {color: #553333; text-decoration: underline;} .signature{ background-color: #E6DED1 ; text-align:left; font-size: 10px; }</style>' 


== Introduction ==

Ce jeu est jouable librement et gratuitement depuis internet, ou mieux en l'installant sur votre ordinateur.

Vous pouvez le t�l�charger � cette adresse, pour y rejouer par la suite : http://informfr.tuxfamily.org/

Ce projet a �t� r�alis� en vue d'�tre pr�sent� dans le mus�e Suisse de la Science-Fiction, le Mus�e d'Ailleurs 
(http://www.ailleurs.ch/), pour l'exposition "An exhibition of Unspeakable Things", � l'occasion du 70�me anniversaire de la mort de H.P. Lovecraft, un auteur de romans fantastiques et d'horreur am�ricain.

Le th�me principal est en rapport avec les id�es et travaux d�velopp�s dans l'�bauche de texte "The Commonplace Book", et cela s'inscrit donc dans le mythe de Cthulhu popularis� par cet �crivain. 


== Comment jouer ? ==


=== D�marrage rapide du jeu ===

Pour d�buter le jeu sous MS Windows, veuillez simplement cliquer sur le fichier ex�cutable "Lieux Communs.exe" (celui avec l'ic�ne de tentacule verte). Le reste des instructions et des aides figurent dans le jeu lui-m�me. Sur les autres syst�mes (Mac OS X, Linux etc), vous pourrez jouer au jeu en r�cup�rant un interpr�teur pour votre plateforme et en ouvrant avec celui-ci le fichier lieuxcommuns.blb.

Vous trouverez plus d'aide sur la mise en route � cette adresse : http://ifiction.free.fr/index.php?id=aide 



=== � propos des fictions interactives ===

Un bref rappel sur le fonctionnement des fictions interactives :

Dans les jeux de fiction interactive, l'interaction avec le jeu se fait uniquement par l'interm�diaire du clavier de l'ordinateur, en tapant les commandes ad�quates, par exemple "allumer la lampe", "prendre le livre" etc.

Vous pouvez vous r�f�rer � la commande "aide" durant le jeu pour en apprendre plus sur les diverses possibilit�s et limitations de cette aventure.

Pour en savoir plus � propos des fictions interactives (comment y jouer, comment en cr�er), veuillez vous rendre sur notre site : http://ifiction.free.fr 


=== Les commandes usuelles ===

Ces commandes ne sont pas exhaustives, mais devraient permettre de parcourir la plupart du jeu. 

allumer .... / 
attacher .... � .... / 
attaquer .... / 
bruler .... / 
casser .... / 
creuser .... / 
descendre / 
donner .... � ....  / 
dormir / 
embrasser .... / 
entrer / 
entrer dans .... / 
est / 
eteindre .... / 
examiner .... / 
fouiller .... / 
invoquer .... / 
lire .... / 
mettre .... sur .... / 
monter sur .... / 
nord / 
ouest / 
ouvrir .... / 
parler avec .... / 
piloter .... / 
poser .... sur .... / 
prendre .... / 
regarder .... / 
regarder sous .... / 
secouer .... / 
sentir .... / 
sortir / 
sud / 
toucher .... / 
tourner .... / 
utiliser .... sur .... / 

Bonne chance !


== Auteurs ==

- JB 
 - R�daction, Code, Images
 - lejibe {a.t} gmail com 
 - http://www.attracteurs-etranges.com

- Eric Forgeot 
 - R�daction, Code, Images, Sons
 - eforgeot {a.t} yahoo fr
 - http://eric.spritewood.net

- Hugo Labrande 
 - R�daction, Code, Id�es
 - mulehollandaise {a.t} msn com 
 - http://mulehollandaise.canalblog.com

- Samuel Verschelde (Stormi) 
 - Code, ajout de fonctionnalit�s, d�boguage et corrections, beta-tests, quelques descriptions mineures
 - stormi {a.t} laposte net 

- R�mi Verschelde (Akien)
 - correction orthographique et syntaxique, quelques bribes de codes, ajouts de d�tails pour la mimesis, tests et id�es
 - rverschelde {a.t} hotmail fr

- Julien Caillaud (Stab)
 - Couverture (image d'introduction)
 - darkstab {a.t} hotmail com
 - http://stabalarash.com/

- Jean-Luc Pontico 
 - r�alisation de la biblioth�que fran�aise pour le moteur de jeu Inform, conseils techniques
 - http://jlpo.free.fr/

- Gr�goire Schneller (Eriorg)
 - Beta-testing et contributions diverses
 - eriorg {a.t} hotmail com



== Historique ==


L'exposition est pr�vue pour octobre 2007.
Ce projet a �t� initi� le 20 mars 2007 et la majeure partie a �t� termin�e � la mi-juin.

Pour plus de renseignement les fils du forum � propos de ce projet sont :

La proposition du jeu : http://ifiction.free.fr/forumBB/viewtopic.php?t=263
Les participants : http://ifiction.free.fr/forumBB/viewtopic.php?t=268
Installation d'un d�p�t subversion : http://ifiction.free.fr/forumBB/viewtopic.php?t=267
http://www.illuminatedlantern.com/if/games/lovecraft/


== � propos du th�me de ce jeu ==

Les id�es exprim�es dans cette histoire ne sauraient �tre interpr�t�es comme exprimant une prise de position ou les opinions des r�dacteurs de la communaut� de la fiction interactive francophone, ni comme un mod�le � suivre pour qui que ce soit.

Si vous pensez que ce jeu ne correspond pas � vos propres opinions et morale, ou � celui
de vos proches, nous vous encourageons � ne pas en poursuivre la lecture. N�anmoins, nous
nous sommes assur�s d'avoir un texte d�cent et convenable, et respectant un peu le style de Lovecraft.


== Syst�me ==

Le syst�me de jeu utilis� est Inform 6 : http://www.inform-fiction.org/inform6.html


=== Pr�cisions techniques === 

Le fichier de jeu est ind�pendant du type d'ordinateur ou de syst�me que vous avez, que cela soit windows, macintosh ou autre. En revanche, il vous faudra trouver un interpr�teur en fonction de votre syst�me, et parfois �galement en fonction du type de jeu. Par exemple pour lire un document en vid�o sur ordinateur il faut utiliser un type de logiciel (lecteur vid�o), et pour lire un document de texte il en faut un autre (traitement de texte). Mais heureusement pour la plupart des jeux il existe des interpr�teurs qui savent les lire tous. 
Vous veillerez donc � d�marrer l'interpr�teur en premier, et � ouvrir le jeu � partir de l�. Par la suite, avec les associations de fichiers g�r�es par votre ordinateur, il sera sans doute possible de d�marrer directement l'interpr�teur et le jeu souhait� en cliquant sur l'ic�ne du jeu. Ou bien selon l'interpr�teur, d'utiliser une liste de favoris pour rejouer ou continuer un jeu.

� noter : nous recommandons l'utilisation de Gargoyle comme interpr�teur ( http://ccxvii.net/gargoyle/ ) par contre il peut utiliser deux "moteurs", l'un est GIT (par d�faut), mais qui est (�tait ?) bugg�, l'autre est GLULXE (livr� avec gargoyle, mais non utilis�). Veuillez utiliser la version GLULXE avec le jeu, et non pas GIT de Gargoyle. Mais si vous utiliser l'ic�ne de d�marrage, cela d�marrera le jeu correctement de toute fa�on.

=== Compilation des sources ===

Tous les outils de compilation sont fournis dans ce package.
(Ces outils sont librement diffusables cf. http://www.inform-fiction.org/source/index.html) 

==== Windows ====

Sous windows la compilation se fait en lan�ant le fichier compilation_glulx.bat ou 
compilation_zmachine.bat et le fichier jouable pour tester sera cpb.ulx 
(par exemple avec le logiciel interpr�teur glulxe.exe), ou cpb.z8 (� lancer avec jouer_zmachine.bat)

==== Linux ==== 

Sous linux la compilation se fait en lan�ant le script compile_game_zmachine.sh et en testant le 
fichier r�sultant lieuxcommuns.z8 avec l'interpr�teur frotz par exemple.

==== Autres syst�mes ====

Les compilateurs inform existent sous d'autres syst�mes (Mac OS X, *BSD, Amiga, BeOS...) 
mais ils ne sont pas fournis dans ces sources, veuillez vous r�f�rer au site d'Inform 
pour les r�cup�rer pour votre syst�me.


=== Binaire ===

A cette adresse se trouve une version r�cente du binaire de l'aventure en d�veloppement :
- http://informfr.tuxfamily.org/lieuxcommuns/lieuxcommuns.blb (version multimedia)
- http://informfr.tuxfamily.org/lieuxcommuns/lieuxcommuns.z8 (version texte)


== Les fichiers de travail ==

Les fichiers de l'aventure qu'il est possible de modifier sont :

cpb.inf
classes.inf
verbes.inf
les fichiers de la forme scene##.inf 

vous pouvez cr�er de nouvelles sous-aventures sous la forme
scene##.inf, avec le num�ro de la scene correspondant.

Pour les rapports de bugs c'est ici :
http://informfr.tuxfamily.org/flyspray/

Pour avoir une liste des sc�nes possibles, allez voir � cette adresse :
http://www.lapetiteclaudine.com/archives/011196.html

Les fichiers en *.h dans le dossier lib ne sont pas � modifier en th�orie, 
car ce n'est pas le but de ce projet (sauf pour corriger des bugs dans
la biblioth�que.)
Les fichiers FrenchG1PSP.h et French1PSP.h peuvent �tre modifi�, m�me si � terme l'inclusion
de ces fichiers se fera sur le projet http://viewvc.tuxfamily.org/svn_informfr_informfr/trunk/


=== G�n�rer le jeu ===

%Pour transformer le fichier cpb.ulx en contenu multimedia, il faut le copier dans le 
%dossier media, et lancer le fichier gblorb.ulx, charger cpb.res et g�n�rer le fichier cpb.blb 
%� partir de l�. 

==== Format texte ====

tapez "make z5" pour cr�er le jeu au format zcode (texte uniquement)


==== Format multimedia ====

tapez :

- make blb
- make glulx
- make blb

pour cr�er le fichier lieuxcommuns.blb offrant le jeu avec les sons et les images.

Un fichier dans le dossier media, "procedure_pour_creer_un_fichier_multimedia.txt", 
explique comment faire dans le d�tail.

Veuillez noter pour avoir la musique, pour le moment vous devez r�cup�rer les morceau � cet endroit :
** http://ifiction.free.fr/lovecraft/sons/
** nouvelle adresse � d�finir plus tard...
Nous ne la mettons pas dans le gestionnaire de version svn pour �conomiser de la place.


== Cr�dits ==

=== Cr�dit photographique ===

Veuillez noter que les photos �tant sous licence Creative Commons BY-NC-SA, vous ne pouvez vendre la version
glulx de ce jeu (lieuxcommuns.blb).

cr�dit photographique, licence Creative Commons by-nc-sa 
http://creativecommons.org/licenses/by-nc-sa/2.0/

- rscottjones / http://flickr.com/photo_zoom.gne?id=424668195&size=l / Chalet interieur 1
- antoine_ / http://flickr.com/photo_zoom.gne?id=22105358&size=l / Chalet �t�
- kelownabc / http://flickr.com/photo_zoom.gne?id=389776129&size=l  / col
- Andy O'Donnell / http://flickr.com/photo_zoom.gne?id=486155438&size=o / col 2
- Wes & Eli / http://flickr.com/photo_zoom.gne?id=106265798&size=l / chalet interieur 2
- ParaScubaSailor / http://flickr.com/photo_zoom.gne?id=5112281&size=l / chalet hiver
- mafleen / http://flickr.com/photo_zoom.gne?id=64754109&size=o / feu
- Trinity / http://flickr.com/photo_zoom.gne?id=893380&size=o / gardien mus�e (cire)
- arna_�sp / http://flickr.com/photo_zoom.gne?id=356122487&context=set-72157594479559700&size=l / montagne 
- mharrsch / http://flickr.com/photo_zoom.gne?id=1352088&size=o / drap (faune)
- claudecf / http://flickr.com/photos/bip/10471870/ / faune
- Stewart Leiwakabessy / http://flickr.com/photos/stewiedewie/162636177/ / rotonde 
- entr'acte /  http://flickr.com/photo_zoom.gne?id=1650606&size=o / jardin
- MuddylaBoue / http://flickr.com/photo_zoom.gne?id=270537124&size=l / jardin
- Djof / http://flickr.com/photo_zoom.gne?id=208765248&size=l / bassin
- mharrsch / http://www.flickr.com/photo_zoom.gne?id=411559023&size=o / homme cro-magnon
- Leo Reynolds / http://flickr.com/photo_zoom.gne?id=75706929&size=l / ice block
- oisch / http://flickr.com/photo_zoom.gne?id=87019350&size=o  / ice block
- nico97492 / http://flickr.com/photo_zoom.gne?id=236560873&size=o / block glace
- ctd 2005 / http://flickr.com/photo_zoom.gne?id=163173449&size=l / bronze doors 
- antmoose / http://flickr.com/photo_zoom.gne?id=42275968&size=l / bronze doors
- shapeshift / http://flickr.com/photo_zoom.gne?id=501496223&size=o / necropole
- gaetanku / http://flickr.com/photo_zoom.gne?id=478535437&size=m / sapins
- jasonsisk / http://flickr.com/photo_zoom.gne?id=281596450&size=l / livres
- calips96 / http://flickr.com/photo_zoom.gne?id=326461389&size=l / zebre
- Marcus Ramberg / http://flickr.com/photo_zoom.gne?id=124382866&size=l / nymphe
- barcoder96 / http://flickr.com/photo_zoom.gne?id=46975754&size=l / bassin 2
- Neil Carrey / http://flickr.com/photo_zoom.gne?id=134085985&size=l / desert
- AKorour / http://flickr.com/photo_zoom.gne?id=350776636&size=o / desert
- boskizzi / http://www.flickr.com/photos/boskizzi/8873730/ / maison abandonn�e
- vetcw3 / http://flickr.com/photos/cowalsh/323582262/ / snowglobe
- thoth92 / http://flickr.com/photos/aaronf/232628490/in/set-72157594240446551/ / bureau
% - chriswatkins / http://flickr.com/photos/chriswatkins/498865338/
- sheeshoo / http://flickr.com/photos/sheeshoo/15552360/ / sarcophage
- polanri / http://flickr.com/photos/polanri/87348329/in/set-72057594049256550/ vieux mur
- Stewart / http://flickr.com/photo_zoom.gne?id=433289900&size=l / squid (site)

- http://freephotos.se

- http://www.gimp.org (traitement d'images)




=== cr�dit sonore === 

- http://www.sound-fishing.net/  (bruitages)
- Otto Grimwald (Eric Forgeot) : musiques (schism tracker) et samples (zynaddsubfx)


=== Merci et salutations �galement � ===

- Tuxfamily.org
- L'�quipe Inform et en particulier Graham Nelson
- Peter Nepstad pour l'initiative du projet 
- Simon Baldwin pour gblorb
- Tor Andersson et Ben Cressey pour Gargoyle
- Les contributeurs sur RAIF
- La communaut� Inform espagnole et italienne
- Et � tous les joueurs !


== Licences ==

This work is published under a double licence :

The source code is BSD licence (see below)

The pictures, musics and sounds are Creative Commons BY-NC-SA 
(http://creativecommons.org/licenses/by-nc-sa/2.0/)

It means you can use the source code and the zmachine game (text only : lieuxcommuns.z8) as you wish, but the glulx version (multimedia : lieuxcommmuns.blb) which contains the pictures can't be used for commercial purposes (i.e. resell it).

```
Copyright (c) 2007 <ifiction.free.fr>

Tous droits r�serv�s. 
La redistribution et l'utilisation sous forme binaire ou de code source, 
avec ou sans modification, est autoris�e � condition que les conditions 
suivantes soient respect�es:
 
1. Les redistributions du code source doivent conserver l'indication de 
   copyright ci-dessus, cette liste de conditions et la renonciation suivante. 
2. Les redistributions sous forme binaire doivent reproduire l'indication 
   de copyright ci-dessus, cette liste de conditions et la renonciation suivante 
   dans la documentation et/ou les autres accessoires fournis avec la distribution. 
3. Toutes les publicit�s mentionnant les caract�ristiques ou l'utilisation 
   de ce logiciel doivent mentionner la phrase suivante : 
     "Ce produit inclus un logiciel et du travail d�velopp�s par la communaut� 
   de la fiction interactive francophone (http://ifiction.free.fr/) et 
   internationale, ainsi que par les cr�ateurs du syst�me INFORM 
   (http://www.inform-fiction.org)."
4. Les noms des cr�ateurs et d�veloppeurs ne peuvent �tre utilis�s pour 
   approuver ou promouvoir les produits d�riv�s de ce logiciel sans une 
   permission �crite sp�cifique pr�alable.

CE LOGICIEL EST FOURNI PAR LES TITULAIRES DU COPYRIGHT ET LES PARTICIPANTS
"TEL QUEL" ET TOUTES LES GARANTIES EXPLICITES OU IMPLICITES, Y COMPRIS, 
MAIS NON LIMIT�ES A CELLES-CI, LES GARANTIES IMPLICITES DE COMMERCIALISATION 
ET D'AD�QUATION � UN USAGE PARTICULIER SONT D�NI�ES.
EN AUCUN CAS LES TITULAIRES DU COPYRIGHT OU LES PARTICIPANTS NE PEUVENT 
�TRE TENUS POUR RESPONSABLE DES DOMMAGES DIRECTS, INDIRECTS, FORTUITS, 
PARTICULIERS, EXEMPLAIRES OU CONS�CUTIFS � UNE ACTION (Y COMPRIS, MAIS NON 
LIMIT�S � CEUX-CI, L'ACQUISITION DE MARCHANDISES OU DE SERVICES DE REMPLACEMENT; 
LES PERTES D'UTILISATIONS, DE DONN�ES OU FINANCI�RES; OU L'INTERRUPTION 
D'ACTIVIT�S) DE QUELQUE MANI�RE QUE CES DOMMAGES SOIENT CAUS�S ET CECI 
POUR TOUTES LES TH�ORIES DE RESPONSABILIT�S, QUE CE SOIT DANS UN CONTRAT, 
POUR DES RESPONSABILIT�S STRICTES OU DES PR�JUDICES (Y COMPRIS DUS � UNE 
NEGLIGENCE OU AUTRE CHOSE) SURVENANT DE QUELQUE MANI�RE QUE CE SOIT EN DEHORS 
DE L'UTILISATION DE CE LOGICIEL, M�ME EN CAS D'AVERTISSEMENT DE LA POSSIBILIT� 
DE TELS DOMMAGES.
```

```
BSD type licence 
Copyright (c) 2007 <ifiction.free.fr>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement: 
   "Ce produit inclus un logiciel et du travail d�velopp�s par la communaut� 
   de la fiction interactive francophone (http://ifiction.free.fr/) et internationale, 
   ainsi que par les cr�ateurs du syst�me INFORM (http://www.inform-fiction.org)."
4. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
```






